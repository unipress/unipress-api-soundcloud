<?php
/**
 * Main PHP file used to for initial calls to UniPress API Add-on for SoundCloud classes and functions.
 *
 * @package UniPress API
 * @since 1.0.0
 */
 
/*
Plugin Name: UniPress API Add-on for SoundCloud
Plugin URI: http://getunipress.com/
Description: A premium WordPress plugin by UniPress.
Author: UniPress Development Team
Version: 1.0.0
Author URI: http://getunipress.com/
Tags:
*/

//Define global variables...
if ( !defined( 'UNIPRESS_STORE_URL' ) )
	define( 'UNIPRESS_STORE_URL',	'http://getunipress.com' );
	
define( 'UPAPI_SOUNDCLOUD_NAME', 			'UniPress API Add-on for SoundCloud' );
define( 'UPAPI_SOUNDCLOUD_SLUG', 			'unipress-api-soundcloud' );
define( 'UPAPI_SOUNDCLOUD_VERSION', 		'1.0.0' );
define( 'UPAPI_SOUNDCLOUD_DB_VERSION', 		'1.0.0' );
define( 'UPAPI_SOUNDCLOUD_URL', 			plugin_dir_url( __FILE__ ) );
define( 'UPAPI_SOUNDCLOUD_PATH', 			plugin_dir_path( __FILE__ ) );
define( 'UPAPI_SOUNDCLOUD_BASENAME', 		plugin_basename( __FILE__ ) );
define( 'UPAPI_SOUNDCLOUD_REL_DIR', 		dirname( UPAPI_SOUNDCLOUD_BASENAME ) );

define( 'UPAPI_SOUNDCLOUD_IOS_MAX_CHAR', 	218 ); //characters
define( 'UPAPI_SOUNDCLOUD_ANDROID_MAX_CHAR', 4000 ); //characters


/**
 * Instantiate UniPress API class, require helper files
 *
 * @since 1.0.0
 */
function unipress_api_soundcloud_plugins_loaded() {
	
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

	if ( is_plugin_active( 'unipress-api/unipress-api.php' ) ) {

		require_once( 'class.php' );
	
		// Instantiate the Pigeon Pack class
		if ( class_exists( 'UniPress_API_SoundCloud' ) ) {
			
			global $unipress_api_soundcloud;
			
			$unipress_api_soundcloud = new UniPress_API_SoundCloud();
			
			require_once( 'functions.php' );
			require_once( 'shortcodes.php' );
					
			//Internationalization
			load_plugin_textdomain( 'unipress-api-soundcloud', false, UPAPI_SOUNDCLOUD_REL_DIR . '/i18n/' );
				
		}
	
	} else {
	
		add_action( 'admin_notices', 'unipress_api_soundcloud_requirement_nag' );
		
	}

}
add_action( 'plugins_loaded', 'unipress_api_soundcloud_plugins_loaded', 4815162342 ); //wait for the plugins to be loaded before init