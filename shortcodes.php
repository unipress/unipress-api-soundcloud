<?php
/**
 * Registers UniPress API Add-on for SoundCloud shortcodes
 *
 * @package UniPress API Add-on for SoundCloud
 * @since 1.0.0
 */

 if ( !function_exists( 'do_unipress_soundcloud' ) ) { 

	/**
	 * Shortcode for UniPress to hide content that you don't want to show on the app
	 *
	 * @since 1.0.0
	 */
	function do_unipress_soundcloud( $atts ) {
		
		$settings = get_unipress_api_soundcloud_settings();
		$content = '';
		$defaults = array(
			'url' => '',
		);
	
		// Merge defaults with passed atts
		// Extract (make each array element its own PHP var
		$args = shortcode_atts( $defaults, $atts );
		
		if ( !empty( $_GET['unipress-api'] ) || !$settings['mobile-only'] ) { //Only show this in UniPress API Calls
			if ( !empty( $args['url'] ) ) {
				
				$head = wp_remote_head( $args['url'] . '/stream?client_id=' . $settings['client-id'] );
				$headers = wp_remote_retrieve_headers( $head );

				if ( !empty( $headers['location'] ) ) {
					$content = do_shortcode( '[audio src="' . urldecode( $headers['location'] ) .'"]' );
				}

				// WordPress Adds garbage to the end of this that looks like this - &_=1 (&#038;_=1)... we need to remove it for this to work!
				$content = preg_replace( '/&_=\d+/', '', $content );
				$content = preg_replace( '/&#038;_=\d+/', '', $content );

			}
		}
		
		return $content;
		
	}
	add_shortcode( 'unipress_soundcloud', 'do_unipress_soundcloud' );
	
}
