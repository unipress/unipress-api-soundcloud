<?php
/**
 * Registers UniPress API class
 *
 * @package UniPress API
 * @since 1.0.0
 */

/**
 * This class registers the main issuem functionality
 *
 * @since 1.0.0
 */
if ( ! class_exists( 'UniPress_API_SoundCloud' ) ) {
	
	class UniPress_API_SoundCloud {
		
		/**
		 * Class constructor, puts things in motion
		 *
		 * @since 1.0.0
		 */
		function __construct() {
			
			add_action( 'admin_menu', array( $this, 'admin_menu' ), 15 );

		}
		
		function admin_menu() {
									
			add_submenu_page( 'unipress-settings', __( 'SoundCloud', 'unipress-api-soundcloud' ), __( 'SoundCloud', 'unipress-api-soundcloud' ), apply_filters( 'manage_unipress_api_settings', 'manage_options' ), 'unipress-soundcloud-settings', array( $this, 'settings_page' ) );
			
		}
		
		/**
		 * Get UniPress API options
		 *
		 * @since 1.0.0
		 */
		function get_settings() {
			
			$defaults = array( 
				'client-id' 	=> '',
				'client-secret' => '',
				'mobile-only' 	=> true,
			);
		
			$defaults = apply_filters( 'unipress_api_soundcloud_settings_defaults', $defaults );
			
			$settings = get_option( 'unipress-api-soundcloud' );
												
			return wp_parse_args( $settings, $defaults );
			
		}
		
		/**
		 * Update UniPress API options
		 *
		 * @since 1.0.0
		 */
		function update_settings( $settings ) {
			
			update_option( 'unipress-api-soundcloud', $settings );
			
		}
		
		/**
		 * Create and Display IssueM settings page
		 *
		 * @since 1.0.0
		 */
		function settings_page() {
			
			// Get the user options
			$settings = $this->get_settings();

			if ( isset( $_REQUEST['unipress_api_soundcloud_settings_nonce'] ) 
				&& wp_verify_nonce( $_REQUEST['unipress_api_soundcloud_settings_nonce'], 'save_unipress_api_soundcloud_settings' ) ) {
									
				if ( !empty( $_REQUEST['client-id'] ) ) {
					$settings['client-id'] = $_REQUEST['client-id'];
				} else {
					$settings['client-id'] = '';
				}
				
				if ( !empty( $_REQUEST['client-secret'] ) ) {
					$settings['client-secret'] = $_REQUEST['client-secret'];
				} else {
					$settings['client-secret'] = '';
				}
				
				if ( !empty( $_REQUEST['mobile-only'] ) ) {
					$settings['mobile-only'] = true;
				} else {
					$settings['mobile-only'] = false;
				}
				
				if ( !empty( $errors ) ) {
					foreach( $errors as $error ) {
						echo $error;
					}
				} else {
					$this->update_settings( $settings );
					// update settings notification ?>
					<div class="updated"><p><strong><?php _e( 'UniPress SoundCloud Settings Updated.', 'unipress-api-soundcloud' );?></strong></p></div>
					<?php
				}
				
				do_action( 'update_unipress_api_soundcloud_settings', $settings );
				
			}
						
			// Display HTML form for the options below
			?>
			<div class=wrap>
            <div style="width:70%;" class="postbox-container">
            <div class="metabox-holder">	
            <div class="meta-box-sortables ui-sortable">
            
                <form id="issuem" method="post" action="">

                    <h2 style='margin-bottom: 10px;' ><?php _e( 'UniPress SoundCloud Settings', 'unipress-api-soundcloud' ); ?></h2>
                
                    <?php do_action( 'unipress_api_soundcloud_settings_form_start', $settings ); ?>
                    
                    <div id="modules" class="postbox">
                    
                        <div class="handlediv" title="Click to toggle"><br /></div>
                        
                        <h3 class="hndle"><span><?php _e( 'API Credentials', 'unipress-api-soundcloud' ); ?></span></h3>
                        
                        <div class="inside">
                        
                        <table id="unipress_administrator_options" class="unipress-table">

                        	<tr>
                                <th style="width: 150px;"><?php _e( 'Client ID', 'unipress-api-soundcloud' ); ?></th>
                                <td>
                                	<input type="text" id="client-id" class="regular-text" name="client-id" value="<?php echo htmlspecialchars( stripcslashes( $settings['client-id'] ) ); ?>" />
                                </td>
                            </tr>
                        	<tr>
                                <th><?php _e( 'Client Secret', 'unipress-api-soundcloud' ); ?></th>
                                <td>
                                	<input type="text" id="client-secret" class="regular-text" name="client-secret" value="<?php echo htmlspecialchars( stripcslashes( $settings['client-secret'] ) ); ?>" />
                                </td>
                            </tr>
                        	<tr>
                                <th><?php _e( 'Mobile Only?', 'unipress-api-soundcloud' ); ?></th>
                                <td>
                                	<input type="checkbox" id="mobile-only" name="mobile-only" <?php checked( $settings['mobile-only'] ); ?> />
                                	<p class="description"><?php _e( 'The UniPress SoundCloud shortcode content will only appear on the mobile app, unless this is disabled.', 'unipress-api-soundcloud' ); ?></p>
                                </td>
                            </tr>
                            
                        </table>
                                                                          
                        <p class="submit">
                            <input class="button-primary" type="submit" name="update_unipress_api_settings" value="<?php _e( 'Save Settings', 'unipress-api-soundcloud' ) ?>" />
                        </p>

                        </div>
                        
                    </div>
                    
                    <?php wp_nonce_field( 'save_unipress_api_soundcloud_settings', 'unipress_api_soundcloud_settings_nonce' ); ?>
                                               
                    <?php do_action( 'unipress_api_soundcloud_settings_form_end', $settings ); ?>
                    
                </form>
                
            </div>
            </div>
            </div>
			</div>
			<?php
			
		}	

	}

}
