<?php
 
if ( !function_exists( 'get_unipress_api_soundcloud_settings' ) ) {

	/**
	 * Helper function to get UniPress API settings for current site
	 *
	 * @since 1.0.0
	 *
	 * @return mixed Value set for the issuem options.
	 */
	function get_unipress_api_soundcloud_settings() {
	
		global $unipress_api_soundcloud;
		
		return $unipress_api_soundcloud->get_settings();
		
	}
	
}